### Step 1

Define environment variables for the Microservice in the Dockerfile:

    ENV GEM_HOME=/gembox
    ENV GEM_PATH=/gembox
    ENV BUNDLE_PATH=/gembox
    ENV BUNDLE_APP_CONFIG=/gembox

### Step 2

Uncomment `bundle install` in the same Dockerfile and remove `rails assets:precompile`



### Step 3

Make a new folder named `script` and a new file in this folder named `startup.sh` which contains:

    #!/bin/bash
    rake assets:precompile
    bundle check || bundle install
    bundle exec rails s -b 0.0.0.0
