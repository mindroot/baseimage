FROM ruby:2.6.3

# Set environment variables
ENV GEM_HOME=/gembox
ENV GEM_PATH=/gembox
ENV BUNDLE_PATH=/gembox
ENV BUNDLE_APP_CONFIG=/gembox

#Install docker and yarn
RUN apt-get -y update && apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common && \
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
curl -sL https://deb.nodesource.com/setup_8.x | bash && \
apt-get -y update && apt-get -y install docker-ce build-essential libpq-dev nodejs yarn && \
gem install dpl


COPY Gemfile .

RUN bundle install
